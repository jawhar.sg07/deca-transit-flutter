import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

var url = "https://a360.business-mania.services/";
//signIn void

Future signIn(String username, password) async {
  final prefs = await SharedPreferences.getInstance();
  var data = {'username': username, 'password': password};
  print(data);
  var jsonResponse = null;

  final http.Response response =
      await http.post(url + 'api/login', body: json.encode(data));
  var test = response.body;
//print(test);
  //print(json.decode(response.body));
  if (response.statusCode == 200) {
    print('hereeeee');
    prefs.setString('token', ((response.body)));

    return true;
  } else {
    print('not here');

    return false;
  }
}
// getOrder void

getOrders() async {
  final prefs = await SharedPreferences.getInstance();
  var token = json.decode(prefs.getString('token'));
  var x = (prefs.getString('token'));

  var m = (json.decode(x));
  var retreived_token = m['token'];

  var response = await http.get(
    url + 'api/orders',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $retreived_token',
    },
  );
  return json.decode((response.body));
}

getPalettes(ref) async {
  final prefs = await SharedPreferences.getInstance();
  var x = (prefs.getString('token'));

  var m = (json.decode(x));
  var retreived_token = m['token'];
  var queryParameters = {
    'reference': '$ref',
  };
  var uri = Uri.https(
      'a360.business-mania.services', '/api/order/pallets/', queryParameters);

  var response = await http.get(
    uri,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $retreived_token',
    },
  );
  print(json.decode(response.body));
  return json.decode((response.body));
}

getPackages(ref) async {
  print('ref paleet$ref');
  final prefs = await SharedPreferences.getInstance();
  var x = (prefs.getString('token'));

  var m = (json.decode(x));
  var retreived_token = m['token'];
  var queryParameters = {
    'reference': '$ref',
  };
  var uri = Uri.https(
      'a360.business-mania.services', '/api/pallet/parcels', queryParameters);
  var response = await http.get(
    uri,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $retreived_token',
    },
  );
  return json.decode((response.body));
}
