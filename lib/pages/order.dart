import 'package:decatransit/pages/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:decatransit/http_service.dart';
import 'package:flutter/scheduler.dart';
import '../pages/palette.dart';
class Order extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var orders = [];
  var _isLoading = true;
  SharedPreferences sharedPreferences;

  int counter = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });

    getOrders().then((data) {
      print('hereee ibit order');
      setState(() {
        orders = data['orders'];
        _isLoading = false;
      });

      print(orders);
    });
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => Home()),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => getOrders());
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Image(
                image: AssetImage('assets/lg.png'),
                height: 35,
              ),
              Text('Liste Des Ordres', style: TextStyle(fontSize: 14.0))
            ],
          ),
          leading: GestureDetector(
            onTap: () {
              _scaffoldKey.currentState.openDrawer();
            },
            child: Icon(
              Icons.menu,
              size: 40,
            ),
          ),
        ),
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: Colors.white,
                valueColor: new AlwaysStoppedAnimation(Colors.blue),
              ))
            : Container(
                child: ListView.separated(
                  padding: const EdgeInsets.all(8),
                  itemCount: orders.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: InkWell(
                        splashColor: Colors.blue.withAlpha(10),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context) => Palette(),
                            settings: RouteSettings(arguments:orders[index]['reference']),
                          )
                          );


                        },
                        child: Container(

                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            width: MediaQuery.of(context).size.width * 0.8,

                            child: Stack(
                              children: <Widget>[

                                Align(
                                  alignment: Alignment(0, 0.85),
                                  child: Container(

                                    padding: EdgeInsets.only(
                                        left: 5, top: 12, right: 5, bottom: 1),
                                    width:
                                        MediaQuery.of(context).size.width * 1,
                                    height: 120,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          color: orders[index]['status']['code'] == 'CLOSED'
                                    ? Colors.grey
                                              :
                                        orders[index]['status']['code'] == 'COMPLETED'?
                                          Hexcolor('#F3F6C6')
                                          :
                                        orders[index]['status']['code'] == 'MISSING'?
                                          Hexcolor('#FFD500')
                                        :
                                       
                                        Hexcolor('#F3F6C6')
                                          ,
                                        padding:EdgeInsets.all(10),
                                        child:Row(

                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[

                                            Text(
                                              'Réf:  ',
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 16,
                                                color: Colors.blue,
                                              ),
                                            ),
                                            Text(
                                                '${orders[index]['reference']}')
                                          ],
                                        ),
                                        ),
                                        Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 0)),
                                        Divider(
                                          color: Colors.black,
                                          endIndent: 5,
                                          thickness: 0,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    'Statut: ',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 16,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                  Text(
                                                      '${orders[index]['status']['code']}', style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 17,
                                                    color: Colors.black,
                                                  ),),

                                                ],
                                              ),
                                            ),
                                            orders[index]['status']['code'] == 'MISSING' ||orders[index]['status']['code'] == 'COMPLETED'||orders[index]['status']['code'] == 'ADDED'?
                                            Container(

                                              child: FlatButton(
                                                  onPressed: () async {
                                                    print('here btn inside');
                                                  },

                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .end,
                                                      children: [
                                                        Icon(
                                                            Icons
                                                                .assignment_turned_in,
                                                            color:
                                                                Colors.white),
                                                        Text(
                                                          'Clôturer',
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Montserrat',
                                                            fontSize: 16,
                                                            color: Colors.white,
                                                          ),
                                                        )
                                                      ],
                                                    ),

                                                  color: Colors.blue),
                                            ):
                                            Container(

                                            )
                                            ,
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ),
                    );

                    /* Container(
                    height: 60,
                    color: orders[index]['status']['code'] == 'CLOSED'
                        ? Colors.red
                        : Hexcolor('#F3F6C6'),
                    child: Center(
                        child: Text('Réf: ${orders[index]['reference']}')),
                  );*/
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                ),
              ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Image(
                  image: AssetImage('assets/lg.png'),
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                leading: Icon(Icons.home, color: Colors.blue),
                title: Text('Accueil', style: TextStyle(fontSize: 16.0)),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                title: Text('Déconnexion', style: TextStyle(fontSize: 16.0)),
                leading: Icon(Icons.logout, color: Colors.blue),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ));
  }
}
