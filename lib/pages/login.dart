import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../pages/order.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:decatransit/http_service.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _isLoading = false;
  final username = TextEditingController();
  final password = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  _onSignIn() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
    }
  }

  loggedInUser() async {
    print('hereeeee');
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('token');
    print('ysier $user');
    if (user == null) {
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Order()));
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) =>loggedInUser());

  }

//SignIn action

  /*Future<void> signIn(String username, password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {'username': username, 'password': password};
    print(data);
    var jsonResponse = null;

    final response = await http.post(
        "https://a360.business-mania.services/api/login",
        body: json.encode(data));
    print(response.body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      sharedPreferences.setString("token", jsonResponse['token']);
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Order()), (Route<dynamic> route) => false);

      //sharedPreferences.setString("token", jsonResponse['token']);
    } else {
      print(response.body);

      _showDialog(response.body);

      setState(() {
        _isLoading = false;
      });
    }
  }*/

  void _showDialog(String text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("$text.message"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Hexcolor('#0098F1'),
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Image(
                  image: AssetImage('assets/lg.png'),
                  height: 50,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    controller: username,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Username',
                      prefixIcon: Icon(Icons.person, color: Colors.blue),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide(color: Colors.white)),
                    )),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    obscureText: true,
                    controller: password,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock, color: Colors.blue),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Mot de passe',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide(color: Colors.white)),
                    )),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: _isLoading
                    ? Center(
                        child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation(Colors.blue),
                      ))
                    : FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        onPressed: () async {
                          /*if (_formKey.currentState.validate()) {
                            setState(() {
                              _isLoading = true;
                            });

                            final prefs = await SharedPreferences.getInstance();
                            await signIn(username.text, password.text);


                          } else {
                            setState(() {
                              _isLoading=false;
                            });

                          }*/
                          setState(() {
                            _isLoading = true;
                          });
                          final prefs = await SharedPreferences.getInstance();
                          await signIn(username.text, password.text);
                          if (prefs.getString('token') == null) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home()));
                          } else {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (BuildContext context) => Order()),
                                (Route<dynamic> route) => false);
                          }
                          setState(() {
                            _isLoading = false;
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Text(
                            'Se conencter',
                            style: TextStyle(
                                fontSize: 16.0, color: Hexcolor('#0098F1')),
                          ),
                        ),
                        color: Colors.white),
              )
            ],
          ),
        ));
  }
}
