import 'package:decatransit/pages/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:decatransit/http_service.dart';
import 'package:flutter/scheduler.dart';

class Packages extends StatefulWidget {
  @override
  _PackagesState createState() => _PackagesState();
}

class _PackagesState extends State<Packages> with TickerProviderStateMixin {
  AnimationController _animationController;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _textInputController = TextEditingController();
  var code_pal;
  var packages;
  var _isLoading = true;
  var total_parcels = 0;
  var parcels_scanned = 0;

  @override

  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 4600),
    );
    _animationController.animateTo(2.0);


    Future.delayed(const Duration(seconds: 0), () {
      setState(() {
        code_pal = ModalRoute.of(context).settings.arguments;
      });
      getPackages(code_pal).then((data) {
        print(data);
        setState(() {
          _isLoading = false;
          packages = data['parcels'];
          parcels_scanned = data['totalWaitingParcels'];
          total_parcels = data['pallet']['totalParcels'];
        });
      });
    });
  }
@override
  void dispose() {
  _animationController.dispose();

  super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image(
              image: AssetImage('assets/lg.png'),
              height: 30,
            ),
            Text('Packages scanés: $parcels_scanned/ $total_parcels',
                style: TextStyle(fontSize: 14.0))
          ],
        ),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor: new AlwaysStoppedAnimation(Colors.blue),
            ))
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Réf-Palette:',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Montserrat',
                                    color: Colors.blueAccent),
                              ),
                              Text('$code_pal')
                            ],
                          )),
                      Padding(
                        padding: EdgeInsets.all(15.0),
                        child: FlatButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0)),
                            onPressed: () async {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  var nb = 0;

                                  return StatefulBuilder(
                                    builder: (context, setState) {
                                      return AlertDialog(
                                          title: Text(
                                            'Commnencer le scan',
                                            style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 18,
                                              color: Colors.blue,
                                            ),
                                          ),
                                          content: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                constraints:
                                                BoxConstraints(maxHeight: 100),
                                                child: SingleChildScrollView(
                                                  child: TextField(
                                                    decoration: InputDecoration(
                                                      filled: true,
                                                      fillColor: Color(0xFFF2F2F2),
                                                      focusedBorder:
                                                      OutlineInputBorder(
                                                        borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(4)),
                                                        borderSide: BorderSide(
                                                            width: 1,
                                                            color: Colors.blue),
                                                      ),
                                                      hintText:
                                                      "Scanner les codes à barres puis valider le scan",
                                                      hintStyle: TextStyle(
                                                          fontSize: 16,
                                                          color: Color(0xFFB3B1B1)),
                                                    ),
                                                    controller:
                                                    _textInputController,
                                                    autofocus: true,
                                                    maxLines: null,
                                                    onChanged: (text) {

                                                   var x= '\n'.allMatches(text).length;
                                                   print('ddddddddjsdjhqd $x');

                                                      setState(() {
                                                        nb= x;
                                                      });
                                                    },
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                children: [
                                                  Text('NB packages scanés:'),
                                                  Text(
                                                    ' $nb/$total_parcels',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 16,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              RaisedButton(
                                                  color: Colors.blue,
                                                  textColor: Colors.white,
                                                  child: Text("Valider Scan"),
                                                  onPressed:(){

                                                  })
                                            ],
                                          ));
                                    },
                                  );
                                },
                              );

                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Icon(Icons.qr_code_scanner,
                                    color: Colors.white),
                                Text(
                                  'Scanner',
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                            color: Colors.blue),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: ListView.separated(
                    padding: const EdgeInsets.all(8),
                    itemCount: packages.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        child: InkWell(
                          splashColor: Colors.blue.withAlpha(10),
                          onTap: () {},
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                              ),
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment(0, 0.85),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 5,
                                          top: 12,
                                          right: 5,
                                          bottom: 1),
                                      width:
                                          MediaQuery.of(context).size.width * 1,
                                      height: 120,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        color: Colors.white,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            color: packages[index]['status']
                                                        ['code'] ==
                                                    'NOT_CHECKED'
                                                ? Colors.white
                                                : packages[index]['status']
                                                            ['code'] ==
                                                        'CHECKED'
                                                    ? Hexcolor('#F3F6C6')
                                                    : packages[index]['status']
                                                                ['code'] ==
                                                            'NEW'
                                                        ? Hexcolor('#A4E7F4')
                                                        : Hexcolor('#FFF'),
                                            padding: EdgeInsets.all(10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'Réf:  ',
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 16,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                Text(
                                                    '${packages[index]['reference']}')
                                              ],
                                            ),
                                          ),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 0)),
                                          Divider(
                                            color: Colors.black,
                                            endIndent: 5,
                                            thickness: 0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Text(
                                                      'Statut: ',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 16,
                                                        color: Colors.blue,
                                                      ),
                                                    ),
                                                    Text(
                                                      '${packages[index]['status']['label']}',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 17,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                  child:
                                                  IconButton(
                                                    icon: Icon(
                                                      Icons.remove_red_eye_sharp,
                                                      color: Colors.blue,
                                                      size: 35,

                                                    ),
                                                    onPressed: () {
                                                      showDialog(
                                                          context: context,
                                                          builder: (_) => new AlertDialog(
                                                            title: new Text("Contenu de package", style: TextStyle(
                                                                fontSize: 18,
                                                                fontFamily: 'Montserrat',
                                                                color: Colors.blueAccent),
                                                            ),
                                                            content: new Text('${packages[index]['description']}'),
                                                            actions: <Widget>[
                                                              FlatButton(
                                                                child: Text('Fermer'),
                                                                onPressed: () {
                                                                  Navigator.of(context).pop();
                                                                },
                                                              )
                                                            ],
                                                          ));
                                                    },
                                                  )
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      );
                      /* Container(
        height: 60,
        color: orders[index]['status']['code'] == 'CLOSED'
            ? Colors.red
            : Hexcolor('#F3F6C6'),
        child: Center(
            child: Text('Réf: ${orders[index]['reference']}')),
      );*/
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                  ),
                )
              ],
            ),
    );
  }
}
