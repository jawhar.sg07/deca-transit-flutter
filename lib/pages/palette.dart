import 'package:decatransit/pages/login.dart';
import 'package:decatransit/pages/packages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:decatransit/http_service.dart';
import 'package:flutter/scheduler.dart';

class Palette extends StatefulWidget {
  @override
  _PaletteState createState() => _PaletteState();
}

class _PaletteState extends State<Palette> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var ref;
  var palettes;
  var _isLoading = true;
  var nb=0;
var new_pal=0;
  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    Future.delayed(const Duration(seconds: 0), () {
      setState(() {
        ref = ModalRoute.of(context).settings.arguments;
      });
      getPalettes(ref).then((data) {
        setState(() {
          palettes = data['order']['pallets'];
          nb=data['order']['totalPallets'];
          new_pal=data['order']['totalNewPallets'];
          _isLoading = false;
        });
        print('all palets ggg $palettes');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image(
              image: AssetImage('assets/lg.png'),
              height: 30,
            ),
            Text('Palettes à vérifier : $new_pal/$nb', style: TextStyle(fontSize: 14.0))
          ],
        ),

      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor: new AlwaysStoppedAnimation(Colors.blue),
            ))
          : Container(
              child: ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: palettes.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(10),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Packages(),
                              settings: RouteSettings(
                                  arguments: palettes[index]['reference']),
                            ));
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment(0, 0.85),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 5, top: 12, right: 5, bottom: 1),
                                  width: MediaQuery.of(context).size.width * 1,
                                  height: 120,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        color: palettes[index]['status']['code'] == 'NEW'
                                            ? Colors.white
                                            :
                                        palettes[index]['status']['code'] == 'MISSING'?
                                        Hexcolor('#FFD500')
                                            :
                                        palettes[index]['status']['code'] == 'COMPLETED'?
                                        Hexcolor('#F3F6C6')
                                            :

                                        Hexcolor('#F3F6C6')
                                        ,
                                        padding: EdgeInsets.all(10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Réf:  ',
                                              style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 16,
                                                color: Colors.blue,
                                              ),
                                            ),
                                            Text(
                                                '${palettes[index]['reference']}')
                                          ],
                                        ),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(bottom: 0)),
                                      Divider(
                                        color: Colors.black,
                                        endIndent: 5,
                                        thickness: 0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  'Statut: ',
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 16,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                Text(
                                                  '${palettes[index]['status']['label']}',
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 17,
                                                    color: Colors.black,
                                                  ),
                                                ),


                                              ],
                                            ),
                                          ),
                                          Container(

                                              child: FlatButton(
                                                  shape: new RoundedRectangleBorder(
                                                      borderRadius: new BorderRadius.circular(30.0)),
                                                  onPressed: () async {
                                                    print('here btn inside');
                                                  },

                                                  child: Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .end,
                                                    children: [
                                                      Icon(
                                                          Icons
                                                              .remove_red_eye_rounded,
                                                          color:
                                                          Colors.white),
                                                      Text(
                                                        'Voir détails',
                                                        style: TextStyle(
                                                          fontFamily:
                                                          'Montserrat',
                                                          fontSize: 16,
                                                          color: Colors.white,
                                                        ),
                                                      )
                                                    ],
                                                  ),

                                                  color: Colors.blue)
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                  );

                  /* Container(
                    height: 60,
                    color: orders[index]['status']['code'] == 'CLOSED'
                        ? Colors.red
                        : Hexcolor('#F3F6C6'),
                    child: Center(
                        child: Text('Réf: ${orders[index]['reference']}')),
                  );*/
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              ),
            ),
    );
  }
}
